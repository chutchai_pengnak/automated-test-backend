package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    List<Student> students;
    List<Student> ownStudents;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp1",2.33));
        students.add(new Student("124","B","temp2",2.5));
        students.add(new Student("125","C","temp3",3.0));
        students.add(new Student("126","D","temp4",3.7));
        students.add(new Student("127","F","temp5",4.0));

        ownStudents = new ArrayList<>();
        ownStudents.add(new Student("123","A","temp",2.33));
        ownStudents.add(new Student("132","B","temps",2.5));
        ownStudents.add(new Student("213","C","temper",2.6));
        ownStudents.add(new Student("231","D","tempers",2.7));
        ownStudents.add(new Student("345","F","tempered",4.0));
        ownStudents.add(new Student("354","G","weather",3.0));
        ownStudents.add(new Student("534","F","sky",3.5));
        ownStudents.add(new Student("453","G","moon",4.0));
    }


    @Override
    public List<Student> findAll() {
        return this.students;
    }
    @Override
    public List<Student> findOwnStudent() {
        return this.ownStudents;
    }


}
