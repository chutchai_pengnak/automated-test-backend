package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public Student findStudentById(String id) {
        for (Student student: this.studentDao.findAll()
             ) {
            if (student.getStudentId().equals(id)){
                return student;
            }
        }
        //9.2
        throw new NoDataException();
    }
    //5.3
    @Override
    public Student findName(String name) {
        for (Student student: this.studentDao.findAll()
        ) {
            if (student.getName().equals(name)){
                return student;
            }
        }
        return null;
    }
    @Override
    public Student findsurname(String surname) {
        for (Student student: this.studentDao.findAll()
        ) {
            if (student.getSurName().equals(surname)){
                return student;
            }
        }
        return null;
    }
    @Override
    public Student findgpa(double gpa) {
        for (Student student: this.studentDao.findAll()
        ) {
            if (student.getGpa().equals(gpa)){
                return student;
            }
        }
        return null;
    }


    //5.3

    //5.4
    @Override
    public double getAverageGpa() {
        double total = 0;
        if (this.studentDao.findAll().size() == 0 ) {
            throw new DataDividedByZero();
        }
        for (Student student: this.studentDao.findAll()
        ) {
            total += student.getGpa();

        }
        return total/this.studentDao.findAll().size();
    }

    @Override
    public List<Student> findStudentByPartOfId(String id) throws OutputArrayIsZero{
        List<Student> output = new ArrayList<>();
        for (Student student: this.studentDao.findAll()
                ) {
            if (student.getStudentId().indexOf(id) != -1){
                output.add(student);
            }

        }
        //9.3
        if(output.size() == 0) {
            throw new OutputArrayIsZero();
        }
        return output;
    }

    // ownStudent
    @Override
    public Student findOwnStudentById(String id) {
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            if (student.getStudentId().equals(id)){
                return student;
            }
        }
        return null;
    }
    @Override
    public List<Student> findOwnStudentByPartOfId(String id) {
        List<Student> output = new ArrayList<>();
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            if (student.getStudentId().indexOf(id) != -1){
                output.add(student);
            }
        }
        return output;
    }
    @Override
    public double getAverageGpaOwnStudent() {
        double total = 0;
        for (Student student: this.studentDao.findOwnStudent()
        ) {
            total += student.getGpa();

        }
        return total/this.studentDao.findOwnStudent().size();
    }

}
