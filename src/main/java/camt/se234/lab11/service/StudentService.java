package camt.se234.lab11.service;

import camt.se234.lab11.entity.Student;

import java.util.List;

public interface StudentService {
    Student findStudentById(String id);
    Student findName(String name);
    Student findsurname(String surname);
    Student findgpa(double gpa);

    List<Student> findStudentByPartOfId(String id);
    double getAverageGpaOwnStudent();

    Student findOwnStudentById(String id);
    List<Student> findOwnStudentByPartOfId(String id);
    double getAverageGpa();
}
