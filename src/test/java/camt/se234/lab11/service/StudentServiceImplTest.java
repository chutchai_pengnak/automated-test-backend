package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }
    //  5.1
    @Test
    public void testFindById() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),
                is(new Student("123", "A", "temp1", 2.33)));
        assertThat(studentService.findStudentById("124"),
                is(new Student("124","B","temp2",2.5)));
        assertThat(studentService.findStudentById("125"),
                is(new Student("125","C","temp3",3.0)));
        assertThat(studentService.findStudentById("126"),
                is(new Student("126","D","temp4",3.7)));
    }
    // 5.3
    @Test
    public void test1(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findName("A"),
                is(new Student("123", "A", "temp1", 2.33)));

    }
    @Test
    public void test2(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findsurname("temp1"),
                is(new Student("123", "A", "temp1", 2.33)));

    }
    @Test
    public void test3(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findgpa(2.33),
                is(new Student("123", "A", "temp1", 2.33)));

    }
    // 5.4
    @Test
    public void testGetAverageGPA() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(), is(3.1060000000000003));
    }

    //5.5
    @Test
    public void testAnotherGetAverageGPA() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpaOwnStudent(), is(3.07875));
    }

    //6.2
    @Test
    public void testWithMock(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    //6.3
    @Test
    public void testWithAnotherMock(){
        List<Student> anotherMockStudents = new ArrayList<>();
        anotherMockStudents.add(new Student("123","A","tempers",2.7));
        anotherMockStudents.add(new Student("124","B","tempered",4.0));
        anotherMockStudents.add(new Student("125","C","weather",3.0));
        anotherMockStudents.add(new Student("126","D","sky",3.5));
        anotherMockStudents.add(new Student("127","E","moon",4.0));
        when(studentDao.findOwnStudent()).thenReturn(anotherMockStudents);
        assertThat(studentService.findOwnStudentById("124"),is(new Student("124","B","tempered",4.0)));
    }

    // 6.5
    @Test
    public void testGetAverageGPAWithMock1() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.5));
        mockStudents.add(new Student("125","C","temp",2.6));
        mockStudents.add(new Student("126","D","temp",2.7));
        mockStudents.add(new Student("127","E","temp",4.0));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(), is(2.8259999999999996));
    }

    @Test
    public void testGetAverageGPAWithMock2() {
        List<Student> ownMockStudents = new ArrayList<>();
        ownMockStudents.add(new Student("123","A","temp",2.33));
        ownMockStudents.add(new Student("124","B","temp",2.5));
        ownMockStudents.add(new Student("125","C","temp",2.6));
        ownMockStudents.add(new Student("126","D","temp",2.7));
        ownMockStudents.add(new Student("127","F","temp",4.0));
        ownMockStudents.add(new Student("128","G","temp",3.0));
        ownMockStudents.add(new Student("129","F","temp",3.5));
        ownMockStudents.add(new Student("130","G","temp",4.0));
        when(studentDao.findOwnStudent()).thenReturn(ownMockStudents);
        assertThat(studentService.getAverageGpaOwnStudent(), is(3.07875));
    }

    @Test
    public void testGetAverageGPAWithMock3() {
        List<Student> ownMockStudents = new ArrayList<>();
        ownMockStudents.add(new Student("123","A","temp",2.33));
        ownMockStudents.add(new Student("124","B","temp",2.5));
        ownMockStudents.add(new Student("125","C","temp",2.6));
        ownMockStudents.add(new Student("126","D","temp",2.7));
        ownMockStudents.add(new Student("127","F","temp",4.0));
        ownMockStudents.add(new Student("128","G","temp",3.0));
        ownMockStudents.add(new Student("129","F","temp",3.5));
        ownMockStudents.add(new Student("130","G","temp",4.0));
        when(studentDao.findOwnStudent()).thenReturn(ownMockStudents);
        assertThat(studentService.getAverageGpaOwnStudent(), not(3.0));
    }

    // 7.1
    @Test
    public void testFindByPartOfId() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),
                hasItems(new Student("223","C","temp",2.33),
                new Student("224","D","temp",2.33)));
    }

    // 7.2
    @Test
    public void testAnotherFindByPartOfId() {
        List<Student> anotherMockStudents = new ArrayList<>();
        anotherMockStudents.add(new Student("231","D","tempers",2.7));
        anotherMockStudents.add(new Student("345","F","tempered",4.0));
        anotherMockStudents.add(new Student("344","G","weather",3.0));
        anotherMockStudents.add(new Student("534","F","sky",3.5));
        anotherMockStudents.add(new Student("453","G","moon",4.0));
        when(studentDao.findOwnStudent()).thenReturn(anotherMockStudents);
        assertThat(studentService.findOwnStudentByPartOfId("34"),
                hasItems(new Student("345","F", "tempered",4.0),
                        new Student("344","G","weather",3.0)));
    }

    // 9.3
    @Test(expected = NoDataException.class)
    public void testNoDataException() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp", 2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"), nullValue());
    }
    // 9.4
    @Test(expected = OutputArrayIsZero.class)
    public void testNoDataException2() throws OutputArrayIsZero {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("0"),nullValue());
    }
    // 9.5
    @Test(expected = DataDividedByZero.class)
    public void testGetAverageGPADividedByZero() {
        List<Student> ownMockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(ownMockStudents);
        assertThat(studentService.getAverageGpa(), null);
    }
}